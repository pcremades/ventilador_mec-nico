# Asistente Respiratorio

Desarrollo de un Asistente Respiratorio para colaborar con el sistema salud en Mendoza, Argentina.

![logo](/img/logo.png)

## Artículos recomendables para leer ([fuente](https://github.com/PubInv/covid19-vent-list/blob/master/README.md))

[Requisitos mínimos de ventiladores mecánicos (UK)](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/874279/RMVS001_Rapidly_Manufactured_Ventilator_Specification__PDF.pdf)

[Dispositivo patentado](https://gitlab.com/open-source-ventilator/OpenLung/-/blob/master/research/existing-projects/Bergman_Ambu-bag_Automation_Patent.pdf)

[OSH - Ventilator Machines](https://docs.google.com/document/d/1lZWUwIpN2kINxURqP9Tczn2zYqpuVlBSDBypJt76a2Q/edit?usp=sharing)

## Proyectos

### Los proyectos más difundidos

Estos son los proyectos que más difusión tienen en las redes y que están listos para construirse. Sin embargo, ninguno ha sido probado en pacientes reales.

[Low-Cost Open Source Ventilator](https://github.com/jcl5m1/ventilator)

Quizás el mejor documentado para replicar. La persona que lo desarrolló está constantemente actualizando el diseño. [wiki](https://github.com/jcl5m1/ventilator/wiki).

[Rice OEDK Design: ApolloBVM](https://docs.google.com/document/d/1-DRXnVkJOlDCmvTzh-DgWDxeLSrZTiBYyH0ypzv8tNA/edit?usp=sharing)

Este proyecto está muy bien documentado. No está claro si hay un equipo de personas trabajando activamente en el desarrollo. Ha sido probado con pacientes reales.

[Protofy Team OxyGEN](https://oxygen.protofy.xyz/)

Este es un proyecto activo hasta la fecha (24 de Marzo). [Repositorio](https://github.com/ProtofyTeam/OxyGEN) de documentación.

[Open Source Ventilator - OpenLung BVM Ventilator](https://gitlab.com/open-source-ventilator/OpenLung)

Totalmente abierto bajo licencia GPL. Propone utilizar bolsas AMBU, basándose en un diseño de Rice y el MIT. El autor es Trevor Smale. *No se ha construido* hasta la fecha.

## Noticias importantes

#### 30/03/2020

- [Irlanda en camino a probar ventiladores open source (03/20/20)](https://thehill.com/policy/technology/488637-irish-health-officials-to-review-3d-printed-ventilator)
- [Ventilador de Emergencia del MIT](https://e-vent.mit.edu/)
- [Ingenieros peruanos ofrecen fabricar 100 ventiladores pulmonares](https://larepublica.pe/sociedad/2020/03/22/coronavirus-en-peru-ingenieros-peruanos-ofrecen-fabricar-100-ventiladores-pulmonares/)

### 06/04/2020

- [Perú producirá sus propios respiradores artificiales](https://www.gob.pe/institucion/minsa/noticias/111871-peru-producira-sus-propios-respiradores-artificiales-para-la-atencion-de-pacientes-covid-19)

![Video Marina de Perú](video/VID-20200404-WA0069.mp4)

- [Empresa libera patente de respirador](https://www.medtronic.com/us-en/index.html)

## Documentación

- [Planos constructivos]()
- [Electrónica]()
- [Software]()

## Insumos Electrónica (*Todos precios en proveedor de Mendoza (No hay diferencia significativa con Buenos Aires)*)
- [Arduino MEGA (IT&T sin stock)](http://tienda.ityt.com.ar/placas-base-arduino/734-arduino-mega-2560-r3-atmega2560-con-cable-usb-itytarg.html?search_query=arduino&results=421) ~ $1.500
- [Motor NEMA17](http://tienda.ityt.com.ar/stepper-motor/6100-stepper-bipolar-motor-de-paso-17hs4401-nema17-12v-17a-cnc-3d-cable-itytarg.html?search_query=nema17&results=7) ~ $1.700
- [Shield Pololu](http://tienda.ityt.com.ar/sin-catalogar/236-cnc-shield-v3-s-a4988-pololu-driver-arduino-itytarg.html?search_query=pololu&results=4) ~ $350
~ [Driver Pololu](http://tienda.ityt.com.ar/sin-catalogar/181-modulo-a4988-pololu-driver-motor-stepper-arduino.html?search_query=pololu&results=4) ~ $200
- [Kit Ramps 1.4 Arduino Mega 4 A4988 Pololu](https://articulo.mercadolibre.com.ar/MLA-816266706-kit-ramps-14-arduino-mega-4-a4988-pololu-impresora-3d-_JM#position=1&type=item&tracking_id=21872f6b-3b00-4148-a291-6d330360578a) ~ $3.000
- [Combo Arduino UNO + Shield Pololu + Drivers](https://articulo.mercadolibre.com.ar/MLA-742989865-combo-arduino-uno-con-cnc-shield-4-drv8825-driver-pololu-_JM?quantity=1#position=1&type=item&tracking_id=752bfa44-9419-4eed-8b8d-fad53211a0a1)
