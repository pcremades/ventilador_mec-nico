#include "Arduino.h"
#include <AccelStepper.h>

/* Copyright 2020 Pablo Cremades
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**************************************************************************************
* Autor: Pablo Cremades
* Fecha: 25/03/2020
* e-mail: pablocremades@gmail.com
* Descripción: 
* - Uso la librera StepperDriver para controlar aceleracion
* Change Log:
* 
* To do:
* - usamos alguna interrupcion para parada de emergencia??? Puede ser caulquier endstop (excepto los Z).
*/

#include "pinout.h"

#define MAX_RPM 1000
#define MAX_SPEED MAX_RPM*60/200 //En steps por segundo
#define MOTOR_ACCEL 100
#define MOTOR_DECEL 100

int volume;
int rate;

#define motorInterfaceType 1
//Si Interface=1, entonces usa un Driver, como A4988, pin1=Step_Pin, pin2=Dir_Pin
AccelStepper stepper = AccelStepper(motorInterfaceType, Z_STEP_PIN, Z_DIR_PIN);

void setup(){
 Serial.begin(9600);
 
 pinMode(Z_STEP_PIN , OUTPUT);
 pinMode(Z_DIR_PIN , OUTPUT);
 pinMode(Z_ENABLE_PIN , OUTPUT);
 pinMode(Z_MIN_PIN, INPUT);
 digitalWrite(Z_ENABLE_PIN, LOW);
 stepper.setPinsInverted(false, false, true);
 
 stepper.setPinsInverted(false, false, true);
 stepper.setMaxSpeed(MAX_SPEED);

stepper.setSpeed(-MAX_SPEED/5);  //Bajamos la velocidad para hacer Home
//Buscamos la posicion inicial (HOME)
 while( digitalRead(Z_MIN_PIN) ){
    stepper.runSpeed();
 }
 stepper.stop();
 stepper.setCurrentPosition(0); //Definir esta posicion como cero
 stepper.setSpeed(MAX_SPEED);
 stepper.runToNewPosition(500);  //No se en que unidades esta
}


void loop(){
  volume = analogRead(VOLUME);
  rate = analogRead(RATE);
  
  stepper.setSpeed(MAX_SPEED);
  stepper.runToNewPosition(10);
  delay(500);
  stepper.setSpeed(MAX_SPEED);
  stepper.runToNewPosition(500);
}
