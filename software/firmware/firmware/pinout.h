/* Copyright 2020 Pablo Cremades
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**************************************************************************************
* Autor: Pablo Cremades
* Fecha: 25/03/2020
* e-mail: pablocremades@gmail.co
* Descripción: Hace seguimiento de un objeto basado en sustracción del fondo y un algoritmo de
* decisión bayesiano.
* Con click derecho puede fijar dos puntos de referencia para establecer la escala.
*
* Change Log:
* 
* To do:
* - Que calcule la distancia de referencia y use el dato para corregir la posición.
*
*/

#include "Arduino.h"

//--------------------------------------------> Estos son todos los pines del Shield
#define X_STEP_PIN 54
#define X_DIR_PIN 55
#define X_ENABLE_PIN 38
#define X_MIN_PIN 3
#define X_MAX_PIN 2

#define Y_STEP_PIN 60
#define Y_DIR_PIN 61
#define Y_ENABLE_PIN 56
#define Y_MIN_PIN 14
#define Y_MAX_PIN 15

#define Z_STEP_PIN 46    //---> Usamos solo el eje Z
#define Z_DIR_PIN 48
#define Z_ENABLE_PIN A8  //---- Verificar. Originalmente estaba en 62
#define Z_MIN_PIN 18     //---> Podemos usar los endstop para seguridad
#define Z_MAX_PIN 19

#define E_STEP_PIN 26
#define E_DIR_PIN 28
#define E_ENABLE_PIN 24

#define Q_STEP_PIN 36
#define Q_DIR_PIN 34
#define Q_ENABLE_PIN 30

#define SDPOWER -1
#define SDSS 53
#define LED_PIN 13

#define FAN_PIN 9

#define PS_ON_PIN 12
#define KILL_PIN -1

#define HEATER_0_PIN 10
#define HEATER_1_PIN 8
#define TEMP_0_PIN 13 // ANALOG NUMBERING
#define TEMP_1_PIN 14 // ANALOG NUMBERING
//<--------------------------------------------Hasta aca las definiciones para el shield

//----------------- Pines adicionales
#define VOLUME A5
#define RATE A9
//-----------------------------------

//----------------- Otras constantes
#define MAX_VOLUME
#define MAX_RATE
//----------------------------------

